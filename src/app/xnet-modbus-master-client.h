#include <ucimap.h>
#include "list.h"

struct list_head lh_modbus_device;
struct list_head lh_modbus_query;
struct list_head lh_modbus_register;
struct list_head lh_mqtt;

int init_config(const char* config_path);

struct uci_modbus_device {
    struct ucimap_section_data map;
    struct list_head list1;
    struct list_head alias;

    const char *name;
    const char *type;
    const char *host;
    int port;
    const char *serial_port;
    int baud;
    int data_bit;
    int stop_bit;
    char *mode;
    const char *rts;
    int rts_delay;
    int byte_timeout;
    int response_timeout;
    const char *parity;
};



struct uci_modbus_query {
    struct ucimap_section_data map;
    struct list_head list2;

    const char *name1;
    const char *name;
    int function;
    int start_address;
    int count;
    const char *connection;
    int slave_id;
    int polling_interval;
    const char *register_map;

};


struct uci_modbus_register {
    struct ucimap_section_data map;
    struct list_head list3;

    const char *name1;
    const char *name;
    const char *query_name;
    int address;
    const char *datatype;
    int scaling_a;
    int scaling_b;
    const char *process_or_config;
    int hi_threashold;
    int low_threashold;
    const char *send_alarm;



};


struct uci_mqtt {
    struct ucimap_section_data map;
    struct list_head list4;

    const char *id;
    const char *name;
    const char *host;
    const int port;
    const char *username;
    const char *password;
    const char *data_topic;
    const bool clean_session;
    const char *client_id;
    const char *will_topic;
    const char *will_payload;
    const int will_qos;
    const bool will_retain;
    const int reconnect_delay;
    const int reconnect_delay_max;
    const int reconnect_exponential_backoff;
    const int max_inflight_messages;
};
