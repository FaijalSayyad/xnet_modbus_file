/*
csvfix - reads (possibly malformed) CSV data from input file
         and writes properly formed CSV to output file
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <csv.h>
#include <time.h>
#include <zconf.h>
#include <json-c/json.h>
#include <stdbool.h>
#include "zhelpers.h"

void *subscriber;
void *context;
char *new_contents;

char *query_name;
char *value;
static int k = 0;

/*printing the value corresponding to boolean, double, integer and strings*/
void print_json_value(json_object *jobj) {
    k++;
    printf("\niterartions are %i\n", k);
    enum json_type type;
    printf("type: ", type);
    type = json_object_get_type(jobj); /*Getting the type of the json object*/
    switch (type) {
        case json_type_boolean:
            printf("json_type_boolean");
            printf("value: %sn", json_object_get_boolean(jobj) ? "true" : "false");
            break;
        case json_type_double:
            printf("json_type_double");
            printf("          value: %lf", json_object_get_double(jobj));
            break;
        case json_type_int:
            printf("json_type_int");
            printf("          value: %d", json_object_get_int(jobj));
            break;
        case json_type_string:
            printf("json_type_string");
            printf("          value: %s", json_object_get_string(jobj));
            if (k == 1) {
                query_name = (char *) json_object_get_string(jobj);
            }
            break;

    }

}

void json_parse_array(json_object *jobj, char *key) {
    void json_parse(json_object *jobj); /*Forward Declaration*/
    enum json_type type;

    json_object *jarray = jobj; /*Simply get the array*/
    if (key) {
        jarray = json_object_object_get(jobj, key); /*Getting the array if it is a key value pair*/
        value = (char *) json_object_get_string(jarray);

    }

    int arraylen = json_object_array_length(jarray); /*Getting the length of the array*/
    printf("Array Length: %d", arraylen);
    int i;
    json_object *jvalue;

    for (i = 0; i < arraylen; i++) {
        jvalue = json_object_array_get_idx(jarray, i); /*Getting the array element at position i*/
        type = json_object_get_type(jvalue);
        if (type == json_type_array) {
            json_parse_array(jvalue, NULL);
        } else if (type != json_type_object) {
            printf("value[%d]: ", i);
            print_json_value(jvalue);
        } else {
            json_parse(jvalue);
        }
    }
    k = 0;
}

/*Parsing the json object*/
void json_parse(json_object *jobj) {
    enum json_type type;
    if (jobj != NULL) {

        json_object_object_foreach(jobj, key, val) { /*Passing through every array element*/
            printf("type: ", type);
            type = json_object_get_type(val);
            switch (type) {
                case json_type_boolean:
                case json_type_double:
                case json_type_int:
                case json_type_string:
                    print_json_value(val);
                    break;
                case json_type_object:
                    printf("json_type_objectn");
                    jobj = json_object_object_get(jobj, key);
                    json_parse(jobj);
                    free(jobj);
                    break;
                case json_type_array:
                    printf("type: json_type_array, ");
                    json_parse_array(jobj, key);
                    break;
            }
        }
    }

}


void cb1(void *s, size_t i, void *outfile) {
    csv_fwrite((FILE *) outfile, s, i);
    fputc(',', (FILE *) outfile);
}

void cb2(int c, void *outfile) {
    fseek((FILE *) outfile, -1, SEEK_CUR);
    fputc('\n', (FILE *) outfile);

}

char *get_time() {
    char *time1;
    struct tm *timeinfo;
    time_t rawtime;

    char tm[50];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    sprintf(tm, "%d/%d/%d %d:%d:%d", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900,
            timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

    time1 = tm;
    return time1;
}


int main(int argc, char *argv[]) {
    char buf[1024];
    FILE  *outfile;
    struct csv_parser p;
    csv_init(&p, 0);

    outfile = fopen("/home/ec2x/xnet_modbus_file/test.csv", "wb");
    if (outfile == NULL) {
        fprintf(stderr, "Failed to open file %s: %s\n", argv[2], strerror(errno));
        exit(EXIT_FAILURE);
    }

    sprintf(buf, "id,query,timestamp,value");
    csv_parse(&p, buf, strlen(buf), cb1, cb2, outfile);

    //  Prepare our subscriber
    context = zmq_ctx_new();
    subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://localhost:5555");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "modbus/ftp", 1);
    new_contents = "abc";
    int j = 1;
    char buf2[1024];

    while (j) {
        //  Read envelope with address
        static int id = 1;
        char *address = s_recv(subscriber);
        //  Read message contents
        char *contents = s_recv(subscriber);
        if (strcmp(contents, new_contents) != 0) {
            if (j > 2)
                break;

            new_contents = contents;
            json_object *jobj = json_tokener_parse(new_contents);
            json_parse(jobj);
            snprintf(buf2, 1024, "\n%d,%s,%s,%s", id, query_name, get_time(), value);

            csv_parse(&p, buf2, strlen(buf2), cb1, cb2, outfile);

            printf("[%s]  %s \n", address, new_contents);
            j++;
            id++;

        }

        free(address);
        free(contents);
    }


    csv_fini(&p, cb1, cb2, outfile);
    csv_free(&p);

    fclose(outfile);
    return EXIT_SUCCESS;
}
