#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <list.h>
#include <memory.h>
#include <syslog.h>
#include "xnet-modbus-master-client.h"


//--------------------------------modbus-device-------------------------------------------

static int
modbus_device_init_interface(struct uci_map *map, void *section, struct uci_section *s) {
    struct uci_modbus_device *net = section;

    INIT_LIST_HEAD(&net->list1);
    net->name = s->e.name;
    return 0;
}


static int
modbus_query_init_interface(struct uci_map *map, void *section, struct uci_section *s) {
    struct uci_modbus_query *net = section;

    INIT_LIST_HEAD(&net->list2);
    net->name1 = s->e.name;
    return 0;
}

static int
modbus_register_init_interface(struct uci_map *map, void *section, struct uci_section *s) {
    struct uci_modbus_register *net = section;

    INIT_LIST_HEAD(&net->list3);
    net->name1 = s->e.name;
    return 0;
}

static int modbus_init_mqtt(struct uci_map *map, void *section, struct uci_section *s) {
    struct uci_mqtt *mqtt = section;
    INIT_LIST_HEAD(&mqtt->list4);
    mqtt->name = s->e.name;
    return 0;
}

static int
modbus_device_add_interface(struct uci_map *map, void *section) {
    struct uci_modbus_device *net = section;

    list_add_tail(&net->list1, &lh_modbus_device);

    return 0;
}

static int
modbus_query_add_interface(struct uci_map *map, void *section) {
    struct uci_modbus_query *net = section;

    list_add_tail(&net->list2, &lh_modbus_query);

    return 0;
}

static int
modbus_register_add_interface(struct uci_map *map, void *section) {
    struct uci_modbus_register *net = section;

    list_add_tail(&net->list3, &lh_modbus_register);

    return 0;
}


static int modbus_add_mqtt(struct uci_map *map, void *section) {
    struct uci_mqtt *mqtt = section;
    list_add_tail(&mqtt->list4, &lh_mqtt);
    return 0;
}


static struct ucimap_section_data *
modbus_device_allocate(struct uci_map *map, struct uci_sectionmap *sm, struct uci_section *s) {
    struct uci_modbus_device *p = malloc(sizeof(struct uci_modbus_device));
    memset(p, 0, sizeof(struct uci_modbus_device));
    return &p->map;
}


static struct ucimap_section_data *
modbus_query_allocate(struct uci_map *map, struct uci_sectionmap *sm, struct uci_section *s) {
    struct uci_modbus_query *p = malloc(sizeof(struct uci_modbus_query));
    memset(p, 0, sizeof(struct uci_modbus_query));
    return &p->map;
}


static struct ucimap_section_data *
modbus_register_allocate(struct uci_map *map, struct uci_sectionmap *sm, struct uci_section *s) {
    struct uci_modbus_register *p = malloc(sizeof(struct uci_modbus_register));
    memset(p, 0, sizeof(struct uci_modbus_register));
    return &p->map;
}

static struct ucimap_section_data *
modbus_allocate_mqtt(struct uci_map *map, struct uci_sectionmap *sm, struct uci_section *s) {
    struct uci_mqtt *p = malloc(sizeof(struct uci_mqtt));
    memset(p, 0, sizeof(struct uci_mqtt));
    return &p->map;
}


static struct uci_optmap modbus_device_interface_options[] = {
        {
                UCIMAP_OPTION(struct uci_modbus_device, type),
                .type = UCIMAP_STRING,
                .name = "type"

        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, serial_port),
                .type = UCIMAP_STRING,
                .name = "serial_port"

        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, host),
                .type = UCIMAP_STRING,
                .name = "host"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, port),
                .type = UCIMAP_INT,
                .name = "port"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, baud),
                .type = UCIMAP_INT,
                .name = "baud",

        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, mode),
                .type = UCIMAP_STRING,
                .name = "mode"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, parity),
                .type = UCIMAP_STRING,
                .name = "parity",
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, data_bit),
                .type = UCIMAP_INT,
                .name = "data_bit"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, stop_bit),
                .type = UCIMAP_INT,
                .name = "stop_bit"

        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, rts),
                .type = UCIMAP_STRING,
                .name = "rts"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, rts_delay),
                .type = UCIMAP_INT,
                .name = "rts_delay"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, byte_timeout),
                .type = UCIMAP_INT,
                .name = "byte_timeout"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_device, response_timeout),
                .type = UCIMAP_INT,
                .name = "response_timeout"
        }


};

struct uci_optmap modbus_query_interface_options[] = {
        {
                UCIMAP_OPTION(struct uci_modbus_query, name),
                .type = UCIMAP_STRING,
                .name = "name"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, function),
                .type = UCIMAP_INT,
                .name = "function"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, start_address),
                .type = UCIMAP_INT,
                .name = "start_address"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, count),
                .type = UCIMAP_INT,
                .name = "count"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, connection),
                .type = UCIMAP_STRING,
                .name = "connection"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, slave_id),
                .type = UCIMAP_INT,
                .name = "slave_id"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, polling_interval),
                .type = UCIMAP_INT,
                .name = "polling_interval"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_query, register_map),
                .type = UCIMAP_STRING,
                .name = "register_map"
        }


};

struct uci_optmap modbus_register_interface_options[] = {
        {
                UCIMAP_OPTION(struct uci_modbus_register, address),
                .type = UCIMAP_INT,
                .name = "address"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, name),
                .type = UCIMAP_STRING,
                .name = "name"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, query_name),
                .type = UCIMAP_STRING,
                .name = "query_name"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, datatype),
                .type = UCIMAP_STRING,
                .name = "datatype"

        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, process_or_config),
                .type = UCIMAP_STRING,
                .name = "process_or_config"

        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, send_alarm),
                .type = UCIMAP_STRING,
                .name = "send_alarm"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, scaling_a),
                .type = UCIMAP_INT,
                .name = "scaling_a"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, scaling_b),
                .type = UCIMAP_INT,
                .name = "scaling_b"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, low_threashold),
                .type = UCIMAP_INT,
                .name = "low_threashold"
        },
        {
                UCIMAP_OPTION(struct uci_modbus_register, hi_threashold),
                .type = UCIMAP_INT,
                .name = "hi_threashold"
        }


};

static struct uci_optmap modbus_mqtt_options[] = {
        {UCIMAP_OPTION(struct uci_mqtt, id), .type = UCIMAP_STRING,
                .name = "id", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, host), .type = UCIMAP_STRING,
                .name = "host", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, port), .type = UCIMAP_INT,
                .name = "port", .data.i.max = 32768, .data.i.min= 0,},
        {UCIMAP_OPTION(struct uci_mqtt, username), .type = UCIMAP_STRING,
                .name = "username", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, password), .type = UCIMAP_STRING,
                .name = "password", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, data_topic), .type = UCIMAP_STRING,
                .name = "data_topic", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, clean_session), .type = UCIMAP_BOOL,
                .name = "clean_session",},
        {UCIMAP_OPTION(struct uci_mqtt, client_id), .type = UCIMAP_STRING,
                .name = "client_id", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, will_payload), .type = UCIMAP_STRING,
                .name = "will_payload", .data.s.maxlen = 32,},
        {UCIMAP_OPTION(struct uci_mqtt, will_qos), .type = UCIMAP_INT,
                .name = "will_qos", .data.i.max = 32768, .data.i.min= 0,},
        {UCIMAP_OPTION(struct uci_mqtt, will_retain), .type = UCIMAP_BOOL,
                .name = "will_retain",},
        {UCIMAP_OPTION(struct uci_mqtt, reconnect_delay), .type = UCIMAP_INT,
                .name = "reconnect_delay", .data.i.max = 32768, .data.i.min= 0,},
        {UCIMAP_OPTION(struct uci_mqtt, reconnect_delay_max), .type = UCIMAP_INT,
                .name = "reconnect_delay_max", .data.i.max = 32768, .data.i.min= 0,},
        {UCIMAP_OPTION(struct uci_mqtt, reconnect_exponential_backoff), .type = UCIMAP_BOOL,
                .name = "reconnect_exponential_backoff",},
        {UCIMAP_OPTION(struct uci_mqtt, max_inflight_messages), .type = UCIMAP_INT,
                .name = "max_inflight_messages", .data.i.max = 32768, .data.i.min= 0,},
};


static struct uci_sectionmap modbus_device_interface = {
        UCIMAP_SECTION(struct uci_modbus_device, map),
        .type = "modbus_device",
        .alloc = modbus_device_allocate,
        .init = modbus_device_init_interface,
        .add = modbus_device_add_interface,
        .options = &modbus_device_interface_options[0],
        .n_options = ARRAY_SIZE(modbus_device_interface_options),
        .options_size = sizeof(struct uci_optmap)
};


static struct uci_sectionmap modbus_query_interface = {
        UCIMAP_SECTION(struct uci_modbus_query, map),
        .type = "modbus_query",
        .alloc = modbus_query_allocate,
        .init = modbus_query_init_interface,
        .add = modbus_query_add_interface,
        .options = &modbus_query_interface_options[0],
        .n_options = ARRAY_SIZE(modbus_query_interface_options),
        .options_size = sizeof(struct uci_optmap)
};

static struct uci_sectionmap modbus_register_interface = {
        UCIMAP_SECTION(struct uci_modbus_register, map),
        .type = "modbus_register",
        .alloc = modbus_register_allocate,
        .init = modbus_register_init_interface,
        .add = modbus_register_add_interface,
        .options = &modbus_register_interface_options[0],
        .n_options = ARRAY_SIZE(modbus_register_interface_options),
        .options_size = sizeof(struct uci_optmap)
};

static struct uci_sectionmap modbus_mqtt = {
        UCIMAP_SECTION(struct uci_mqtt, map),
        .type = "mqtt",
        .options = &modbus_mqtt_options[0],
        .n_options = ARRAY_SIZE(modbus_mqtt_options),
        .alloc = modbus_allocate_mqtt,
        .init = modbus_init_mqtt,
        .add = modbus_add_mqtt,
        .options_size = sizeof(struct uci_optmap)
};


struct uci_sectionmap *modbus_smap[] = {
        &modbus_device_interface, &modbus_query_interface, &modbus_register_interface ,&modbus_mqtt

};

struct uci_map modbus_map = {
        .sections = modbus_smap,
        .n_sections = ARRAY_SIZE(modbus_smap),
};

int init_config(const char *config_path) {
    struct uci_context *ctx;
    struct uci_package *pkg;

    INIT_LIST_HEAD(&lh_modbus_device);
    INIT_LIST_HEAD(&lh_modbus_query);
    INIT_LIST_HEAD(&lh_modbus_register);
    INIT_LIST_HEAD(&lh_mqtt);
    ctx = uci_alloc_context();
    if (config_path != NULL) {
        uci_set_confdir(ctx, config_path);
    }
    ucimap_init(&modbus_map);

    if (uci_load(ctx, "xnet-modbus-master-client", &pkg)) {
        printf("Error getting configuration file");
        return 1;
    }
    ucimap_parse(&modbus_map, pkg);

    return 0;
}
