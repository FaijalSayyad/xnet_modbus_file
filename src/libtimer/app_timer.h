//
// Created by aashish on 8/3/20.
//

#ifndef APP_TIMER_H
#define APP_TIMER_H
/*mytimer.h*/
#include <stdlib.h>

typedef enum
{
    TIMER_SINGLE_SHOT = 0, /*Periodic Timer*/
    TIMER_PERIODIC         /*Single Shot Timer*/
} t_timer;

typedef void (*time_handler)(size_t timer_id, void * user_data);

int apptimer_init();
size_t apptimer_start(unsigned int interval, time_handler handler, t_timer type, void * user_data);
void apptimer_stop(size_t timer_id);
void apptimer_finalize();

#endif
