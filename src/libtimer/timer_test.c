//
// Created by aashish on 8/4/20.
//

#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include "app_timer.h"


void timer_handler(size_t timer_id, void * user_data)
{
    printf ("Timer Id %li\n", timer_id);
    printf ("User Data is %s\n", (char*)user_data);
    sleep(5);
}
void main()
{
    apptimer_init();
    printf("Apptimer initialized\n");

    for (int i=0; i<10; i++){
        char *userdata;
        userdata = malloc(10);
        snprintf(userdata, 10, "Hello %i", i);
        apptimer_start(5000, timer_handler, TIMER_PERIODIC, userdata);
    }
    while(true){
        sleep(0.001);

    }

}